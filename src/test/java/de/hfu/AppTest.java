package de.hfu;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.repository.ResidentRepositoryStub;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentService;
import de.hfu.integration.service.ResidentServiceException;
import de.hfu.unit.Queue;
import de.hfu.unit.Util;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.easymock.EasyMock.*;
import static org.junit.jupiter.api.Assertions.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@DisplayName("Test der Klasse App")
public class AppTest {
    @BeforeAll
    static void initAll() {
    }


    @BeforeEach
    void init() {
    }


    @Test
    void succeedingTest() {
        assertEquals(42, 42, "ist immer gleich");
    }

    @Test
    void testIstErstesHalbjahr() {
        assertTrue(Util.istErstesHalbjahr(1));
        assertTrue(Util.istErstesHalbjahr(6));
        assertFalse(Util.istErstesHalbjahr(7));
        assertFalse(Util.istErstesHalbjahr(12));
        try {
            Util.istErstesHalbjahr(-1);
            Util.istErstesHalbjahr(0);
            Util.istErstesHalbjahr(13);
            fail("Erwartete Ausnahme wurde nicht geworfen");
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    void testQueue() {
        try {
            Queue queue = new Queue(0);
            fail("Erwartete Ausnahme wurde nicht geworfen");
        } catch (IllegalArgumentException ignored) {
        }
        Queue queue = new Queue(3);
        try {
            queue.dequeue();
            fail("Erwartete Ausnahme wurde nicht geworfen");
        } catch (IllegalStateException ignored) {
        }
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        assertEquals(1, queue.dequeue());
        assertEquals(2, queue.dequeue());
        assertEquals(4, queue.dequeue());
    }

    @Test
    public void testBaseResidentService() throws ResidentServiceException {
        Resident resident1 = new Resident("Angela", "Merkel", "Hauptstraße", "Berlin", new Date(1960, Calendar.JANUARY, 31));
        Resident resident2 = new Resident("George", "Washington", "Main street", "Washington, D.C.", new Date(1776, Calendar.JULY, 4));
        Resident resident3 = new Resident("Olaf", "Scholz", "Zweitstraße", "Berlin", new Date(1960, Calendar.JULY, 31));
        List<Resident> list = new ArrayList<>();
        list.add(resident1);
        list.add(resident2);
        list.add(resident3);

        ResidentRepository repository = new ResidentRepositoryStub(list);

        BaseResidentService service = new BaseResidentService();
        service.setResidentRepository(repository);

        Resident filter = new Resident(null, null, "*straße", null, null);

        assertEquals(resident1, service.getUniqueResident(resident1));
        assertEquals(resident2, service.getUniqueResident(resident2));
        assertEquals(resident3, service.getUniqueResident(resident3));
        assertNotEquals(resident1, service.getUniqueResident(resident3));
        assertNotEquals(resident2, service.getUniqueResident(resident1));
        assertNotEquals(resident3, service.getUniqueResident(resident2));

        assertEquals(resident1, service.getFilteredResidentsList(filter).get(0));
        assertEquals(resident3, service.getFilteredResidentsList(filter).get(1));
        assertEquals(0, service.getFilteredResidentsList(filter).indexOf(resident1));
        assertEquals(1, service.getFilteredResidentsList(filter).indexOf(resident3));
        assertEquals(2, service.getFilteredResidentsList(filter).size());
        assertFalse(service.getFilteredResidentsList(filter).isEmpty());
    }

    @Test
    public void testBaseResidentServiceMock() throws ResidentServiceException {
        Resident resident1 = new Resident("Angela", "Merkel", "Hauptstraße", "Berlin", new Date(1960, Calendar.JANUARY, 31));
        Resident resident2 = new Resident("George", "Washington", "Main street", "Washington, D.C.", new Date(1776, Calendar.JULY, 4));
        Resident resident3 = new Resident("Olaf", "Scholz", "Zweitstraße", "Berlin", new Date(1960, Calendar.JULY, 31));
        Resident filter = new Resident(null, null, "*straße", null, null);
        List<Resident> list = new ArrayList<>();
        list.add(resident1);
        list.add(resident2);
        list.add(resident3);


        ResidentRepository repositoryMock = createMock(ResidentRepository.class);
        expect(repositoryMock.getResidents()).andReturn(list);
        expect(repositoryMock.getResidents()).andReturn(list);
        expect(repositoryMock.getResidents()).andReturn(list);
        expect(repositoryMock.getResidents()).andReturn(list);
        expect(repositoryMock.getResidents()).andReturn(list);

        replay(repositoryMock);
        BaseResidentService service = new BaseResidentService();
        service.setResidentRepository(repositoryMock);

        service.getUniqueResident(resident1);
        List<Resident> residentList = service.getFilteredResidentsList(filter);
        Resident result1 = service.getUniqueResident(resident1);
        Resident result2 = service.getUniqueResident(resident2);
        Resident result3 = service.getUniqueResident(resident3);
        verify(repositoryMock);

        assertThat(result1, equalTo(resident1));
        assertThat(result2, equalTo(resident2));
        assertThat(result3, equalTo(resident3));

        assertThat(residentList.get(0), equalTo(resident1));
        assertThat(residentList.get(1), equalTo(resident3));
        assertThat(residentList.contains(resident2), is(false));
        assertThat(residentList.size(), is(2));
    }


    @AfterEach
    void tearDown() {
    }

    @AfterAll
    static void tearDownAll() {
    }
}