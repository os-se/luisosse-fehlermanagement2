package de.hfu.integration.repository;

import de.hfu.integration.domain.Resident;

import java.util.ArrayList;
import java.util.List;

public class ResidentRepositoryStub implements ResidentRepository {
    private final List<Resident> list;

    public ResidentRepositoryStub(List<Resident> list) {
        this.list = list;
    }

    @Override
    public List<Resident> getResidents() {
        return list;
    }
}