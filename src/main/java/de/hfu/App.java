package de.hfu;

import java.util.Scanner;

/**
 * Hauptklasse des Praktikumprojekts
 */
public class App {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Gib eine Zeichenkette ein: ");
        String eingabe = scanner.nextLine();
        System.out.println("Die Eingabe in Großbuchstaben: " + eingabe.toUpperCase());

        scanner.close();
    }
}